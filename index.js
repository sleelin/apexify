/*
 * apexify
 * https://bitbucket.org/sleelin/apexify
 *
 * Copyright (c) 2015 S. Lee-Lindsay
 * Licensed under the GNU license.
 */

"use strict";

var Q = require("q"),
    _ = require("lodash"),
    StaticResource = require("./lib/resources"),
    ApexPage = module.exports.ApexPage = require("./lib/apexpage");

function Apexifier() {
    this._sres = [];
    this._done = Q.defer();
}

Apexifier.prototype.addResource = function (filepath, contents) {
    this._sres.push(new StaticResource(filepath, contents));
};

Apexifier.prototype.addBundle = function (filepath) {
    return (_.chain(this._sres).where({dest: filepath}).first().value() || this._sres[this._sres.push(new StaticResource.Bundle(filepath)) - 1]);
};

Apexifier.prototype.processPage = function (file) {
    return this._done.promise.then(function (sres) {
        var page = new ApexPage(file);

        sres.forEach(function (res) {
            page.use(res.ns, (res instanceof StaticResource.Bundle ? res.include(page._deps) : res.dest));
        });

        return page.get();
    });
};

Apexifier.prototype.resolveDependencies = function (cb) {
    var deferred = Q.defer();

    this._done.resolve(this._sres);
    this._done.promise.then(function (sres) {
        sres.forEach(cb);
    }).finally(function () {
        deferred.resolve();
    });

    return deferred.promise;
};

module.exports = Apexifier;
