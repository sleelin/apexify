"use strict";

var $ = require("cheerio"),
    beautifyHtml = require("js-beautify").html,
    config = {
        scripts: {
            clone: $.load("<apex:includeScript></apex:includeScript>", {xmlMode: true}).root(),
            selector: "script[src]:not([src^='http://'])",
            attr: "src",
            target: "body"
        },
        styles: {
            clone: $.load("<apex:styleSheet></apex:styleSheet>", {xmlMode: true}).root(),
            selector: "link[rel='stylesheet']:not([href^='http://'])",
            attr: "href",
            target: "head"
        }
    };

function ApexPage(document) {
    var page = $.load("", {xmlMode: true})("<apex:page></apex:page>").attr({
            docType: "html-5.0",
            applyHtmlTag: "false",
            standardStyleSheets: "false",
            showHeader: "false"
        }).append($("html", document));

    this.contents = page;
    this._deps = Object.keys(config).map(function (source) {
        return $(config[source].selector, page).map(function (index, el) {
            return $(el).attr(config[source].attr);
        }).get();
    }).reduce(function (acc, c) {
        return acc.concat(c);
    });
}

ApexPage.prototype.get = function () {
    return beautifyHtml(this.contents.toString().replace(/\\&apos;/g, "'"), {preserve_newlines: false});
};

ApexPage.prototype.use = function (resourceNS, assets) {
    var page = this.contents,
        apexNS = [["{!URLFOR($Resource.", resourceNS, ", \\'/"].join(""), "\\')}"];

    Object.keys(config).forEach(function (type) {
        var source = config[type],
            target = $(source.target, page);

        $(source.selector, page).filter(function (index, el) {
            return assets.indexOf($(el).attr(source.attr)) >= 0;
        }).each(function (index, el) {
            $(el, page).remove();
            target.append(source.clone.clone().attr("value", [apexNS[0], $(el).attr(source.attr), apexNS[1]].join("")))
        });
    });
};

module.exports = ApexPage;
