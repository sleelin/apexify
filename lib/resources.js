"use strict";

var path = require("path"),
    JSZip = require("jszip");

function StaticResource(filepath, contents) {
    this.dest = filepath;
    this.contents = contents;
    this.ns = path.basename(filepath, path.extname(filepath)).replace(".", "_");
}

StaticResource.prototype.get = function () {
    return this.contents;
};

StaticResource.Bundle = function (filepath) {
    StaticResource.call(this, filepath, new JSZip());
    this._res = {};
};

StaticResource.Bundle.prototype.get = function () {
    return this.contents.generate({type: "nodebuffer", compression: "DEFLATE"});
};

StaticResource.Bundle.prototype.put = function (path, contents) {
    if (path !== undefined && contents !== undefined) {
        this.contents.file(path, contents, {createFolders: true});
    }
};

StaticResource.Bundle.prototype.consider = function (path, contents, forced) {
    if (!!forced) {
        this.put(path, contents);
    } else {
        this._res[path] = contents;
    }
};

StaticResource.Bundle.prototype.include = function (assets) {
    for (var i = 0, paths = [].concat(assets), ilen = paths.length; i < ilen; ++i) {
        this.put(paths[i], this._res[paths[i]]);
    }

    return this.files();
};

StaticResource.Bundle.prototype.files = function () {
    return this.contents.filter(function (path, file) {
        return !file.dir;
    }).map(function (file) {
        return file.name;
    });
};

module.exports = StaticResource;
